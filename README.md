# Thesis
Deals with the resolution of singularities for an arithmetic threefold to a smooth minimal model. Much thanks to my advisor for help with this one, and also the University of Utah. 

## For citation purposes (probably better checked for typos / etc)
- Short version (just contains kernel of idea, that resolving at the surface can be extended to surrounding germ): [https://arxiv.org/abs/1609.08709](https://arxiv.org/abs/1609.08709)
- Tanaka similar paper which is more general, implies the main theorems here afaik, and probably better checked for typos [https://arxiv.org/abs/1608.07676](https://arxiv.org/abs/1608.07676).
- Chapter 4.4-4.6 are better referenced from [https://arxiv.org/abs/1603.04894](https://arxiv.org/abs/1603.04894) if you need it, and aren't really necessary for the rest of the thesis (I threw those in there as they were similar material, but it's not part of the overall flow of the paper).